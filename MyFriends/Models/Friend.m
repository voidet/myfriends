//
//  Friend.m
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import "Friend.h"

@implementation Friend

- (id)initWithDictionary:(NSDictionary *)friendDict {
	if (self = [super init]) {
		self.friendName = friendDict[@"name"];
		self.avatarUrlString = friendDict[@"picture"][@"data"][@"url"];
	}
	return self;
}

@end
