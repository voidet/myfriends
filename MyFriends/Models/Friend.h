//
//  Friend.h
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Friend : NSObject

@property (nonatomic, strong) NSString *profileId;
@property (nonatomic, strong) NSString *friendName;
@property (nonatomic, strong) NSString *avatarUrlString;

- (id)initWithDictionary:(NSDictionary *)friendDict;

@end
