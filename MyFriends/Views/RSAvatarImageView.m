//
//  RSAvatarImageView.m
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import "RSAvatarImageView.h"

@implementation RSAvatarImageView

- (void)awakeFromNib {
	self.layer.cornerRadius = self.frame.size.width / 2;
	self.clipsToBounds = YES;
}

@end
