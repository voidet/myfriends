//
//  RSFriendTableViewCell.m
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import "RSFriendTableViewCell.h"

@interface RSFriendTableViewCell()

@property (nonatomic, strong) IBOutlet UILabel *friendNameLabel;

@end

@implementation RSFriendTableViewCell


- (void)prepareForReuse {
	self.avatarImageView.image = [UIImage imageNamed:@"avatarPlaceholder"];
	self.friendNameLabel.text = @"";
}

- (void)setIsOdd:(BOOL)isOdd {
	_isOdd = isOdd;
	if (isOdd) {
		self.contentView.backgroundColor = [UIColor colorWithRed:(59.0/255) green:(89.0/255) blue:(152.0/255) alpha:1];
	} else {
		self.contentView.backgroundColor = [UIColor colorWithRed:(70.0/255) green:(104.0/255) blue:(176.0/255) alpha:1];
	}
}

- (void)setFriend:(Friend *)friend {
	_friend = friend;
	self.friendNameLabel.text = friend.friendName;
}

@end
