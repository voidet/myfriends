//
//  RSFriendTableViewCell.h
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Friend.h"
#import "RSAvatarImageView.h"

@interface RSFriendTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL isOdd;
@property (nonatomic, strong) IBOutlet RSAvatarImageView *avatarImageView;
@property (nonatomic, strong) Friend *friend;

@end
