//
//  RSLoadMoreFooterView.m
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import "RSLoadMoreFooterView.h"

@interface RSLoadMoreFooterView()

@property (nonatomic, strong) UITapGestureRecognizer *tryAgainGestureRecogniser;

@end

@implementation RSLoadMoreFooterView

- (void)awakeFromNib {
	self.tryAgainGestureRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tryAgain)];
	[self.tryAgainLabel addGestureRecognizer:self.tryAgainGestureRecogniser];
	self.tryAgainLabel.alpha = 0;
	self.tryAgainGestureRecogniser.enabled = NO;
}

- (void)beginLoading {
	self.tryAgainGestureRecogniser.enabled = NO;
	self.tryAgainLabel.alpha = 0;

	self.loadingIndicator.alpha = 0;
	[self.loadingIndicator startAnimating];
	
	[UIView animateWithDuration:0.25 animations:^{
		self.messageLabel.alpha = 1;
		self.loadingIndicator.alpha = 1; //self isn't being capturing here
	}];
}

- (void)tryAgain {
	[self.tryagainDelegate tryAgain];
}

- (void)endLoading {
	self.messageLabel.alpha = 0;
	self.loadingIndicator.alpha = 0;
	// stop animating, view will exist in memory but not visible with no point to keep animating
	[self.loadingIndicator stopAnimating];
}

- (void)showTryAgain {
	[self endLoading];
	
	self.tryAgainLabel.alpha = 1;
	self.tryAgainGestureRecogniser.enabled = YES;
}

@end
