//
//  RSLoadMoreFooterView.h
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RSTryAgainProtocol

- (void)tryAgain;

@end

@interface RSLoadMoreFooterView : UIView

@property (nonatomic, strong) IBOutlet UILabel *messageLabel;
@property (nonatomic, strong) IBOutlet UILabel *tryAgainLabel;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (nonatomic, weak) id<RSTryAgainProtocol> tryagainDelegate;

- (void)beginLoading;
- (void)endLoading;
- (void)showTryAgain;

@end
