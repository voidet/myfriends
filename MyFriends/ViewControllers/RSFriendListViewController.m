//
//  RSFriendListViewController.m
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import "RSFriendListViewController.h"
#import "RSFriendTableViewCell.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Friend.h"
#import "RSLoadMoreFooterView.h"

@interface RSFriendListViewController () <UITableViewDataSource, UITableViewDelegate, RSTryAgainProtocol, UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSMutableArray *friends;
@property (nonatomic, strong) NSString *nextPaginationUrlString;
@property (nonatomic, strong) NSURLSession *urlSession;
@property (nonatomic, assign) BOOL endOfUserListReached;
@property (nonatomic, assign) BOOL loadingInProgress;
@property (nonatomic, strong) IBOutlet RSLoadMoreFooterView *loadMoreView;
@property (nonatomic, strong) UIAlertView *logoutAlertView;

@end

#define FRIEND_ROW_HEIGHT 80

@implementation RSFriendListViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.navigationItem.hidesBackButton = YES;
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	
	self.friends = [NSMutableArray array];
	self.loadMoreView.tryagainDelegate = self;
	self.urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];

	[self loadFacebookFriends];
}

- (IBAction)logout:(id)sender {
	self.logoutAlertView = [[UIAlertView alloc] initWithTitle:@"Logging out?" message:@"Are you sure you want to logout?\nIf so, it was fun while it lasted :)" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, logout", nil];
	[self.logoutAlertView show];
}

- (void)closeFbSessionAndLogout {
	[[FBSession activeSession] closeAndClearTokenInformation];
	[self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark UIAlertView delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (alertView == self.logoutAlertView) {
		if (buttonIndex == 1) {
			[self closeFbSessionAndLogout];
		}
	}
}

#pragma mark Facebook network calls
- (void)loadFacebookFriends {
	[self loadFacebookFriendsWithCompletion:^(BOOL success, NSArray *newFriends) {
		if (success && [newFriends count] > 0) {
			[self reloadTableViewWithNewFriends:newFriends];
		} else {
			[self.loadMoreView showTryAgain];
		}
	}];
}

- (void)reloadTableViewWithNewFriends:(NSArray *)newFriends {
	[self.loadMoreView endLoading];

	NSMutableArray *newIndexPaths = [NSMutableArray array];
	for (NSInteger i = [self.friends count]; i < [self.friends count] + [newFriends count]; i++) {
		[newIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
	}
	
	[self.friends addObjectsFromArray:newFriends];
	[self.tableview insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)tryAgain {
	[self loadFacebookFriends];
}

- (void)handlePagingWithString:(NSString *)nextPagingUrlString {
	// Since this would of been called from a successful call, if there is no next urlstring to paginate
	// with then it is advised that this is the end of the results.
	if (nextPagingUrlString && ![nextPagingUrlString isEqualToString:@""]) {
		self.nextPaginationUrlString = nextPagingUrlString;
	} else {
		// we can unset the footerview now as no need to load more or try again
		self.tableview.tableFooterView = nil;
		self.endOfUserListReached = YES;
		self.nextPaginationUrlString = nil;
	}
}

// If there were more network calls within the app I would of abstracted this out into a separate
// class. Seeing as though it's just the one main VC it is fine here.
- (void)loadFacebookFriendsWithCompletion:(void(^)(BOOL success, NSArray *newFriends))completion {
	self.tableview.tableFooterView = self.loadMoreView;
	
	[self.loadMoreView beginLoading];
	self.loadingInProgress = YES;
	
	NSInteger thumbNailSize = [UIScreen mainScreen].scale * (FRIEND_ROW_HEIGHT - 20); // 20 padding
	NSString *fbAccessToken = [[[FBSession activeSession] accessTokenData] accessToken];
	
	NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/v2.2/me/taggable_friends?limit=10&sort=name&fields=name,picture.width(%i).height(%i)&after=%@&access_token=%@", (int)thumbNailSize, (int)thumbNailSize, self.nextPaginationUrlString, fbAccessToken];
	NSURL *urlToLoad = [NSURL URLWithString:urlString];
	
	NSURLSessionDataTask *task = [self.urlSession dataTaskWithURL:urlToLoad completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
		
		if (!error) {
			
			NSError *jsonError;
			NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
			
			if (jsonError || ![dict objectForKey:@"data"]) {
				// If we're having trouble deciphering the JSON or it doesn't have a friends list (not authorised?)
				// best to mark it as an error for now to try again
				dispatch_async(dispatch_get_main_queue(), ^{
					completion(NO, nil);
				});
				return;
			}
			
			// construct a new array of new friends objetcs
			NSArray *friends = [dict objectForKey:@"data"];
			NSMutableArray *newFriends = [NSMutableArray array];
			if (friends) {
				for (id friend in friends) {
					if ([friend isKindOfClass:NSDictionary.class]) {
						NSDictionary *friendDict = (NSDictionary *)friend;
						Friend *friendObj = [[Friend alloc] initWithDictionary:friendDict];
						[newFriends addObject:friendObj];
					}
				}
			}
			
			// Method call to store the next url to page on or mark end of results
			[self handlePagingWithString:dict[@"paging"][@"next"]];
			
			dispatch_async(dispatch_get_main_queue(), ^{
				completion(YES, newFriends);
			});
			
		} else {
			dispatch_async(dispatch_get_main_queue(), ^{
				completion(NO, nil);
			});
		}
		
		self.loadingInProgress = NO;
	}];
	[task resume];
}

#pragma mark Tableview Datasource delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.friends count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

	static NSString *cellIdentifier = @"friendCell";
	RSFriendTableViewCell *friendCell = (RSFriendTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	Friend *friend = (Friend *)self.friends[indexPath.row];
	friendCell.friend = friend;
	friendCell.isOdd = (indexPath.row % 2 == 1) ? YES : NO;
	
	// I left the network calls for image loading here.
	// I was going to move it to the view to take care of, however down the track we might want the VC
	// to handle a queue of image downloads, and when to cancel if older images are being slow to load
	NSURL *avatarURL = [NSURL URLWithString:friend.avatarUrlString];
	NSURLSessionDataTask *task = [self.urlSession dataTaskWithURL:avatarURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
		
		// Here we ensure the image that's about to be set does in fact belong to that cell
		// this ensures that slow network calls do not come back to haunt us and replace the wrong cell
		if (!error && friendCell.friend == friend) {
			UIImage *avatarImage = [UIImage imageWithData:data];
			dispatch_async(dispatch_get_main_queue(), ^{
				friendCell.avatarImageView.image = avatarImage;
			});
		}
	}];
	[task resume];
	
	return friendCell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return FRIEND_ROW_HEIGHT;
}

// Using scrollViewWillEndDragging allows us to forecast where the user will land.
// If the user will scroll to the last 5 cells then fire off a load more call
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
	
	if (self.loadingInProgress || self.endOfUserListReached) {
		return;
	}
	
	// Determine where we're about to scroll to, get the indexpath's row and see if its in range of preloading
	CGRect toBeVisibleRect = CGRectMake(0, targetContentOffset->y, scrollView.frame.size.width, scrollView.frame.size.height);
	NSArray *indexPaths = [self.tableview indexPathsForRowsInRect:toBeVisibleRect];
	NSIndexPath *lastVisibleCellIndexPath = indexPaths.lastObject;
	
	if (lastVisibleCellIndexPath.row > [self.friends count] - 5) {
		[self loadFacebookFriends];
	}
}

@end