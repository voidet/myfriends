//
//  LoginViewController.m
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import "RSLoginViewController.h"
#import "RSNavigationPushSegue.h"
#import <FacebookSDK/FacebookSDK.h>

@interface RSLoginViewController ()

@property (nonatomic, strong) IBOutlet UIButton *loginButton;
@property (nonatomic, assign) BOOL animateToFriendList;

@end

@implementation RSLoginViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	[self checkForExistingFBSession];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	if ([segue.identifier isEqualToString:@"friendList"]) {
		RSNavigationPushSegue *pushSegue = (RSNavigationPushSegue *)segue;
		pushSegue.animated = self.animateToFriendList;
	}
	
}

- (void)goToFriendListAnimated:(BOOL)animated {
	self.animateToFriendList = animated;
	[self performSegueWithIdentifier:@"friendList" sender:self];
}

- (IBAction)loginButtonPressed:(id)sender {

  // If the session state is any of the two "open" states when the button is clicked
  if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
		
		// Close the session and remove the access token from the cache
		// The session state handler (in the app delegate) will be called automatically
		[FBSession.activeSession closeAndClearTokenInformation];
		
  // If the session state is not any of the two "open" states when the button is clicked
	} else {
		// Open a session showing the user the login UI
		// You must ALWAYS ask for public_profile permissions when opening a session
		[FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"user_friends"]
																			 allowLoginUI:YES
																	completionHandler:
		 ^(FBSession *session, FBSessionState state, NSError *error) {
			 if (!error && state == FBSessionStateOpen) {
				 [self goToFriendListAnimated:YES];
			 } else if (error) {
				 [self showErrorAlertViewWithMessage:[FBErrorUtility userMessageForError:error]];
			 }
		 }];
	}

}

- (void)showErrorAlertViewWithMessage:(NSString *)message {
	if (!message || [message isEqualToString:@""]) {
		message = @"Something went a bit wrong. Please ensure you are connected to the internet and try again";
	}
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oh no!" message:message delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
	[alertView show];
}

#pragma mark Facebook session handling
- (void)checkForExistingFBSession {
	if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
		[FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"user_friends"]
																			 allowLoginUI:NO
																	completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
																		if (!error && state == FBSessionStateOpen) {
																			[self goToFriendListAnimated:NO];
																		} else if (error) {
																			[self showErrorAlertViewWithMessage:[FBErrorUtility userMessageForError:error]];
																		}
																	}];
	}
}


@end
