//
//  AppDelegate.m
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	[FBAppEvents activateApp];
	
	[[UINavigationBar appearance] setTitleTextAttributes:@{
																												 NSFontAttributeName: [UIFont fontWithName:@"Campton-Medium" size:17.0],
																												 NSForegroundColorAttributeName: [UIColor colorWithRed:(59.0/255) green:(89.0/255) blue:(152.0/255) alpha:1]
																												 }];
	[[UIBarButtonItem appearance] setTitleTextAttributes:@{
																												 NSFontAttributeName: [UIFont fontWithName:@"Campton-Medium" size:14.0],
																												 NSForegroundColorAttributeName: [UIColor colorWithRed:(59.0/255) green:(89.0/255) blue:(152.0/255) alpha:1]
																												 } forState:UIControlStateNormal];
	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
	return [FBSession.activeSession handleOpenURL:url];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	[FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
