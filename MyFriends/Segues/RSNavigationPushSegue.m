//
//  RSNavigationPushSegue.m
//  MyFriends
//
//  Created by Richard S on 10/01/2015.
//  Copyright (c) 2015 Richard Sbresny. All rights reserved.
//

#import "RSNavigationPushSegue.h"

@implementation RSNavigationPushSegue

- (void)perform {
	UIViewController *sourceVC = self.sourceViewController;
	UIViewController *destVC = self.destinationViewController;
	
	[sourceVC.navigationController pushViewController:destVC animated:self.animated];
}

@end