#My Friends Notes

Firstly thank you so much for giving me the opportunity to complete this puzzle for you. It was an interesting exercise, as I quickly found out, Facebook has actually disabled the ability to get a user's list of friends since API version 2.0 ([changelog here](https://developers.facebook.com/docs/apps/changelog?locale=en_GB#v2_0_login)). I have somewhat of a work around that instead shows a list of "Taggable friends"

Facebook SDK also had a critical issue getting in the way of ScrollView Performance. This unfortunately took up quite sometime researching. Basically once a user interacts with the tableview, a UIScrollView, the application run loop enters a blocking state for NSURLConnection by going into Event Tracking mode. I was hoping to capture the NSURLConnection object to fire it off onto a different RunLoop but of course Facebook did not expose this variable.

I scrambled at the end to instead use my own NSURLSession call using the user's access token and performance increased impressively.

I also used as little third party tools for this exercise. I would of loved to use AFNetworking & SDWebImage for this exercise however felt it was best to demonstrate my handling of common issues without their assistance.

# Setup

Unfortunately in order to use this on your side my Facebook application would need to go into review, and as this is not public I doubt it would get approved. This is because using "Taggable friends" requires app review and special consideration. Instead there are two options:

* Create your own FB application and update the "FacebookAppID" and "URL types -> URL Schemes" entries
* If you add me as a friend and have a FB developer account I can add you as a developer and you can test without code modifications on your side.

In order to avoid any hassle I have also just taken some screen captures of the experience so you can see the design etc.

#Links

* Video: http://jotlab.com/MyFriends.mp4
* Zip File: http://jotlab.com/MyFriends.zip